package packet

import (
	"github.com/alexgokhale/logger"
	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
)

type PacketInfo struct {
	Packet gopacket.Packet
	Interface string
}

type Channel struct {
	Tunnel chan PacketInfo
	Count int
}

func GetChannel() Channel {
	channel := &Channel{
		Tunnel: make(chan PacketInfo, 1000),
		Count: 0,
	}
	return *channel
}

func (c *Channel) Listen() {
	interfaces, err := pcap.FindAllDevs()
	if err != nil {
		logger.Fatal("Unable to fetch interfaces [ERR: %v]", err)
	}

	for _, i := range interfaces {
		name := i.Name
		handle, err := pcap.OpenLive(name, 262144, true, pcap.BlockForever)

		if err != nil {
			logger.Fatal("Unable to listen to [I-%v] [ERR: %v]", name, err)
		}

		source := gopacket.NewPacketSource(handle, handle.LinkType())

		go func() {
			for packet := range source.Packets() {
				c.Tunnel <- PacketInfo{
					Packet: packet,
					Interface: name,
				}
			}
		}()
	}
}