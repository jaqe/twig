package main

import (
	"encoding/json"
	"net/http"

	"github.com/alexgokhale/logger"
	"github.com/google/gopacket/pcap"
	"github.com/gorilla/websocket"
)

var (
	filter []byte
)

type channel struct {
	conn *websocket.Conn
}

type packet struct {
	Header string      `json:"header"`
	Data   []byte `json:"data"`
}

func main() {
	http.HandleFunc("/socket", func(w http.ResponseWriter, r *http.Request) {
		upgrader := websocket.Upgrader{}
		conn, err := upgrader.Upgrade(w, r, nil)
		handleErr(err)
		channel := &channel{
			conn: conn,
		}
		defer conn.Close()
		for {
			msgType, message, err := conn.ReadMessage()
			handleErr(err)
			channel.handleMessage(msgType, message)
		}
	})
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "html/index.html")
	})
	http.ListenAndServe(":80", nil)
}

func (c *channel) handleMessage(msgType int, msg []byte) {
	logger.Success("Parsing %v [TYPE: %v]", msg, msgType)
	pkt := &packet{}
	err := json.Unmarshal(msg, pkt)
	handleErr(err)
	switch pkt.Header {
	case "INTF?":
		intfs, err := pcap.FindAllDevs()
		handleErr(err)
		_infts, err := json.Marshal(intfs)
		pkt := &packet{
			Header: "INTF",
			Data:   _infts,
		}
		handleErr(err)
		c.conn.WriteJSON(pkt)
	case "FILTER":
		logger.Info("Data: %v", pkt.Data)
		filter = pkt.Data
		logger.Success("Filter: %v", filter)
	case "TEST":
		c.conn.WriteMessage(websocket.TextMessage, []byte("TEST"))
	default:
		return
	}
}

// "line" parameter is literally disgusting. only for debugging purposes, should be removed soon-ish.
func handleErr(err error) bool {
	if err != nil {
		logger.Fatal("Error: %v", err)
	}
	return false
}