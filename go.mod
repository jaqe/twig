module twig

go 1.18

require (
	github.com/alexgokhale/logger v1.1.3
	github.com/google/gopacket v1.1.19
	github.com/gorilla/websocket v1.5.0
)

require (
	github.com/fatih/color v1.13.0 // indirect
	github.com/mattn/go-colorable v0.1.9 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
)
